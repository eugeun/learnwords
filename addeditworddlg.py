from PyQt4.QtGui import (QDialog, QMessageBox, QApplication, 
            QTableWidget, QTableWidgetItem)
from PyQt4.QtCore import (Qt, SIGNAL, QString, QVariant, pyqtSignature)
from worddata import WordData, Translation, WordDataException
from ui import Ui_addeditworddlg

class AddEditWordDlg(QDialog, Ui_addeditworddlg.Ui_AddEditWordDlg):
    
    def __init__(self, words, word=None, parent=None):
        """ It can serve as Add or Edit Dialog. Mode determines by self.word, self.editedWord is what actually form is holding
        """
        super(AddEditWordDlg, self).__init__(parent)
        self.setupUi(self)
        
        self.words = words
        self.word = word
        self.editedWord = WordData()

        for form in WordData.GrammarForms:
            self.grammarFormComboBox.addItem(form)
        
        if self.word is None:
            # Set "Add Word" mode
            self.caption = u"{0} - Add Word".format(unicode(QApplication.applicationName()))
        else:
            # Set "Edit Word" mode
            self.caption = u"{0} - Edit Word".format(unicode(QApplication.applicationName()))
            self.saveButton.setText("Edi&t")
            self.editedWord.copyFrom(self.word)
            self.titleLineEdit.setText(self.editedWord.title)

        self.on_titleLineEdit_textEdited(QString(self.editedWord.title))
        self.updateAllTranslationsTable()
        self.setWindowTitle(self.caption)
    
    
    @pyqtSignature("QString")
    def on_titleLineEdit_textEdited(self, text):
        title_presented = not text.isEmpty()
        self.translationGroupBox.setEnabled(title_presented)

        translation_presented = not self.translationTextEdit.toPlainText().isEmpty()
        example_presented = not self.translationTextEdit.toPlainText().isEmpty()
        
        self.addTranslationButton.setEnabled(title_presented and translation_presented and example_presented)
    
        translation_edited = self.currentTranslation() is not None
        self.editTranslationButton.setEnabled(title_presented and translation_presented and translation_edited and example_presented)
        
        self.removeTranslationButton.setEnabled(title_presented and translation_edited)
        
        word_has_tranlsations = bool(self.editedWord)
        self.allTranslationsTable.setEnabled(title_presented and word_has_tranlsations)
        self.saveButton.setEnabled(title_presented and word_has_tranlsations)
    
    
    @pyqtSignature("")
    def on_translationTextEdit_textChanged(self):
        self.updateAddAndEditTranslationButtons()
    
    
    @pyqtSignature("")
    def on_exampleTextEdit_textChanged(self):
        self.updateAddAndEditTranslationButtons()
    
    
    def updateAddAndEditTranslationButtons(self):
        translation_presented = not self.translationTextEdit.toPlainText().isEmpty()
        example_presented = not self.exampleTextEdit.toPlainText().isEmpty()
        
        self.addTranslationButton.setEnabled(translation_presented and example_presented)
        
        translation_edited = self.currentTranslation() is not None
        self.editTranslationButton.setEnabled(translation_edited and translation_presented and example_presented)
        self.removeTranslationButton.setEnabled(translation_edited and translation_presented and example_presented)
    
    
    @pyqtSignature("")
    def on_addTranslationButton_clicked(self):
        grammarForm = unicode(self.grammarFormComboBox.currentText())
        score = self.scoreSpinBox.value()
        transcription = unicode(self.transcriptionEdit.text()).strip().strip("[]")
        text = unicode(self.translationTextEdit.toPlainText()).strip()
        example = unicode(self.exampleTextEdit.toPlainText()).strip()
        translation = Translation(grammarForm, text, example, transcription, score)
        self.editedWord.title = unicode(self.titleLineEdit.text()).strip()
        try:
            self.editedWord.addTranslation(translation)
        except WordDataException, e:
            QMessageBox.critical(self, "Add translation", unicode(e))
            self.translationTextEdit.selectAll()
            self.translationTextEdit.setFocus()
            return
        self.updateAllTranslationsTable()
        self.saveButton.setEnabled(True)
        self.transcriptionEdit.clear()
        self.scoreSpinBox.setValue(0)
        self.translationTextEdit.clear()
        self.exampleTextEdit.clear()
        self.grammarFormComboBox.setCurrentIndex(0)
        self.grammarFormComboBox.setFocus()
    
    
    def updateAllTranslationsTable(self):
        self.allTranslationsTable.clear()
        self.allTranslationsTable.setColumnCount(5)
        self.allTranslationsTable.setHorizontalHeaderLabels(["Score", "Form", "Transcription", "Translation", "Example"])
        self.allTranslationsTable.setAlternatingRowColors(True)
        self.allTranslationsTable.setEditTriggers(QTableWidget.NoEditTriggers)
        self.allTranslationsTable.setSelectionBehavior(QTableWidget.SelectRows)
        self.allTranslationsTable.setSelectionMode(QTableWidget.SingleSelection)
        self.allTranslationsTable.setRowCount(len(self.editedWord))
        
        if not self.editedWord:
            self.allTranslationsTable.setEnabled(False)
            self.saveButton.setEnabled(False)
            return
        
        self.allTranslationsTable.setEnabled(True)
        for row, translation in enumerate(self.editedWord):
            item = QTableWidgetItem(unicode(translation.score))
            item.setData(Qt.UserRole, QVariant(long(id(translation))))
            self.allTranslationsTable.setItem(row, 0, item)
            item = QTableWidgetItem(translation.grammarForm)
            self.allTranslationsTable.setItem(row, 1, item)

            item = QTableWidgetItem(u"[{0}]".format(translation.transcription) if translation.transcription else "")
            self.allTranslationsTable.setItem(row, 2, item)
            
            item = QTableWidgetItem(translation.text)
            self.allTranslationsTable.setItem(row, 3, item)
            
            item = QTableWidgetItem(translation.example)
            self.allTranslationsTable.setItem(row, 4, item)
            
            self.allTranslationsTable.resizeColumnsToContents()
    
    
    @pyqtSignature("QTableWidgetItem*, QTableWidgetItem*")
    def on_allTranslationsTable_currentItemChanged(self, previous=None, current=None):
        translation = self.currentTranslation()
        if translation is not None:
            i = self.grammarFormComboBox.findText(translation.grammarForm)
            if i < 0:
                i = 0
                QMessageBox.critical(self, "Translation Error", 
                        "The grammar form '{0}' is unknown! "
                        "Chosen the first grammar form. Check it!".format(translation.grammarForm))
            self.grammarFormComboBox.setCurrentIndex(i)
            self.scoreSpinBox.setValue(translation.score)
            self.transcriptionEdit.setText(translation.transcription)
            self.translationTextEdit.setPlainText(translation.text)
            self.exampleTextEdit.setPlainText(translation.example)
    
    
    @pyqtSignature("")
    def on_editTranslationButton_clicked(self):
        translation = self.currentTranslation()
        if translation is not None:
            grammarForm = unicode(self.grammarFormComboBox.currentText())
            transcription = unicode(self.transcriptionEdit.text()).strip().strip("[]")
            score = self.scoreSpinBox.value()
            text = unicode(self.translationTextEdit.toPlainText()).strip()
            example = unicode(self.exampleTextEdit.toPlainText()).strip()
            try:
                self.editedWord.updateTranslation(translation, grammarForm, transcription, score, text, example)            
            except WordDataException, e:
                QMessageBox.critical(self, self.caption, unicode(e))
                self.translationTextEdit.selectAll()
                self.translationTextEdit.setFocus()
                return

            self.updateAllTranslationsTable()
            self.translationTextEdit.clear()
            # TODO: strange bug: we need clean up translationTextEdit twice! Required investigation.
            self.translationTextEdit.clear()
            self.exampleTextEdit.clear()
            self.transcriptionEdit.clear()
            self.scoreSpinBox.setValue(0)
            self.grammarFormComboBox.setCurrentIndex(0)
            self.grammarFormComboBox.setFocus()
    
    
    @pyqtSignature("")
    def on_removeTranslationButton_clicked(self):
        translation = self.currentTranslation()
        if translation is not None:
            reply = QMessageBox.question(self, 
                    self.caption, 
                    u"Remove translation: '{0}'?".format(unicode(translation)), 
                    QMessageBox.Yes|QMessageBox.No)
            if reply == QMessageBox.Yes:
                self.editedWord.deleteTranslation(translation)
                self.updateAllTranslationsTable()
                self.grammarFormComboBox.setCurrentIndex(0)
                self.translationTextEdit.clear()
                self.exampleTextEdit.clear()
    
    
    def currentTranslation(self):
        row = self.allTranslationsTable.currentRow()
        if row < 0:
            return None
        item = self.allTranslationsTable.item(row, 0)
        id = item.data(Qt.UserRole).toLongLong()[0]
        translation = self.editedWord.translationFromId[id]
        return translation
    
    
    def accept(self):
        if self.titleLineEdit.text().isEmpty():
            QMessageBox.warning(self, 
                self.caption, 
                "You should type the Title at first!")
            self.titleLineEdit.setFocus()
            return
        self.editedWord.title = unicode(self.titleLineEdit.text()).strip()
        
        if not self.editedWord:
            QMessageBox.warning(self, 
                self.caption, 
                "You should add at least one Translation!")
            self.grammarFormComboBox.setFocus()
            return
        
        if self.editTranslationButton.isEnabled():
            # We trying to save current translation which is obvious in the self.word
            translation = self.currentTranslation()
            if translation is not None:
                translation.grammarForm = unicode(self.grammarFormComboBox.currentText())
                translation.transcription = unicode(self.transcriptionEdit.text()).strip().strip("[]")
                translation.score = self.scoreSpinBox.value()
                translation.text = unicode(self.translationTextEdit.toPlainText()).strip()
                translation.example = unicode(self.exampleTextEdit.toPlainText()).strip()
            
        try:
            if self.word is None:
                self.words.addWord(self.editedWord)
            else:
                self.words.editWord(self.word, self.editedWord)
        except WordDataException, e:
            QMessageBox.critical(self, self.caption, unicode(e))
            self.titleLineEdit.selectAll()
            self.titleLineEdit.setFocus()
            return
        QDialog.accept(self)
    
    
    def reject(self):
        title_changed = False
        if unicode(self.titleLineEdit.text()).strip() != self.editedWord.title:
            title_changed = True
        translation_changed = False
        if self.editTranslationButton.isEnabled():
            translation = self.currentTranslation()
            if translation is not None:
                if unicode(self.grammarFormComboBox.currentText()) != translation.grammarForm or \
                    unicode(self.transcriptionEdit.text()).strip().strip("[]") != translation.transcription or \
                    self.scoreSpinBox.value() != translation.score or \
                    unicode(self.translationTextEdit.toPlainText()).strip() != translation.text or \
                    unicode(self.exampleTextEdit.toPlainText()).strip() != translation.example:
                        translation_changed = True
        if self.editedWord.dirty or title_changed or translation_changed:
            reply = QMessageBox.question(self, 
                    self.caption, 
                    "Save changes?", 
                    QMessageBox.Yes|QMessageBox.No)
            if reply == QMessageBox.Yes:
                self.accept()
                return
            else:
                QDialog.reject(self)
                return
        # It can occasionally occures in edition mode when target word was empty before
        if not self.titleLineEdit.text().isEmpty() and  not self.editedWord:
            QMessageBox.warning(self, 
                self.caption, 
                "You should add at least one Translation!")
            self.grammarFormComboBox.setFocus()
            return
        QDialog.reject(self)


if __name__ == "__main__":
    import sys
    from worddata import WordContainer
    app = QApplication(sys.argv)
    words = WordContainer()
    word = WordData("Hello")
    translation1 = Translation("verb", "test", "passed")
    translation2 = Translation("verb", "test2", "passed")
    word.addTranslation(translation1)
    word.addTranslation(translation2)
    word.dirty = False
    words.addWord(word)
    for word in words:
        print(u"{0}:".format(unicode(word.title)))
        for translation in word:
            print(u"\t{0}".format(unicode(translation)))
    #form = AddEditWordDlg(words)
    form = AddEditWordDlg(words, word)
    if form.exec_():
        for word in words:
            print(u"{0}:".format(unicode(word.title)))
            for translation in word:
                print(u"\t{0}".format(translation))
