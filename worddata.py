#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-
"""The main thing in this tool"""
from __future__ import print_function
import bisect
import codecs
import os
import copy
import random
import re
from lxml import etree
from mako.template import Template
import mako


class WordDataException(Exception): pass
class TranslationException(WordDataException): pass
class WordContainerException(WordDataException): pass


CODEC="UTF-8"
NEWPARA=unichr(0x2029)
NEWLINE=unichr(0x2028)
APOSTROPHE=unichr(0x2027)
LEFTANGELBRACKET=unichr(0x2329)
RIGHTANGELBRACKET=unichr(0x232A)
WORDDATA_TEMPLATE=r"resources/worddata.mako"


def encodeSpecialSymbols(text):
    return text.replace("\n\n", NEWPARA).replace("\n", NEWLINE).replace("'", APOSTROPHE).replace("<", LEFTANGELBRACKET).replace(">", RIGHTANGELBRACKET)


def decodedSpecialSymbols(text):
    return text.replace(NEWPARA, "\n\n").replace(NEWLINE, "\n").replace(APOSTROPHE, "'").replace(LEFTANGELBRACKET, "<").replace(RIGHTANGELBRACKET, ">")


class Translation(object):
    """ This is the basic translation class. It holds:
    - grammar form of the transaltion
    - transcription
    - text translation
    - example of the use
    - score of learning status(range: 0-100 (%))
    - faults  - amount of wrong answers, proccessed in Translation.addScore() and Translatino.subScore()
    """
    
    def __init__(self, grammarForm, text, example, transcription="", score=0, faults=0):
        """
        >>> translation = Translation("noun", "a spherical object or mass of material", "")
        Traceback (most recent call last):
        ...
        TranslationException: 'example' of the translation should not be empty!
        >>> translation = Translation("noun", "a spherical object or mass of material", "a ball of wool", "", "a12")
        Traceback (most recent call last):
        ...
        TranslationException: Type of 'score' should be only int (was 'a12')!
        """
        self.grammarForm = grammarForm
        if not text:
            raise TranslationException, "'text' of the translation should not be empty!"
        self.text = text
        if not example:
            raise TranslationException, "'example' of the translation should not be empty!"
        self.example = example
        self.transcription = transcription
        try:
            score = int(score)
        except ValueError, e:
            raise TranslationException, u"Type of 'score' should be only int (was '{0}')!".format(unicode(score))
        if score < 0 or score > 100:
            raise TranslationException, "Value of 'score' should be in range: 0..100 (%)"
        self.score = score
        self.faults = faults
    
    
    def __unicode__(self):
        u"""
        >>> translation = Translation("noun", "a spherical object or mass of material", "a ball of wool", u"bɔːl")
        >>> print(unicode(translation))
        noun:[bɔːl]:{a spherical object or mass of material}
        >>> translation = Translation("noun", "a spherical object or mass of material", "a ball of wool")
        >>> print(unicode(translation))
        noun:{a spherical object or mass of material}
        """
        return u"{0}:{1}{{{2}}}".format(
                self.grammarForm, 
                u"[{0}]:".format(self.transcription) if self.transcription else "", 
                unicode(self.text))
    
    
    def __eq__(self, other):
        equal_grammar_forms = bool(self.grammarForm == other.grammarForm)
        if not equal_grammar_forms:
            return False
        
        other_text_lower = other.text.lower().replace("  ",  " ").strip()
        self_text_lower = self.text.lower().replace("  ",  " ").strip()
        if self_text_lower == other_text_lower:
            return True
        
        # go further and try to remove any bracktes in the self.text
        # try to remove something like this: "(abc)def(ghi)"
        self_text_lower = re.sub(r"\([^(]*\)", "", self_text_lower)
        # try to remove something like this: "[abc]def[ghi]"
        self_text_lower = re.sub(r"\[[^[]*\]", "", self_text_lower).replace("  ",  " ").strip().strip('.')
        if self_text_lower == other_text_lower:
            return True
        
        # go further and try to split self.text
        if ";" in self.text:
            for text in self_text_lower.split(";"):
                if other_text_lower == text.strip():
                    return True
            return False
        else:
            return False
    
    
    def addScore(self, step):
        self.score += step/(self.faults+1)
        if self.score > 100:
            self.score = 100
        if self.faults > 0:
            self.faults -= 1
    
    
    def subScore(self, step):
        self.score -= step
        if self.score < 0:
            self.score = 0
        self.faults += 1
    
    
class WordData(object):
    
    GrammarForms=("verb", "noun", "adjective", "adverb", "preposition", "conjunction", "informal", "exclamation")
    
    def __init__(self, title=""):
        self.title = title
        self.translationsWithKey = []
        self.translationFromId = {}
        self.dirty = False
    
    
    def addTranslation(self, translation):
        u"""
        >>> fh_test = StringIO('''<?xml version='1.0' encoding='UTF-8'?> \
        <!DOCTYPE WORDS> \
        <words version='12' groupname=''> \
        <word title='ball'> \
        <translations> \
        <translation grammarform='verb' transcription='' score='0'> \
        <text> \
        form a round shape \
        </text> \
        <example> \
         the fishing nets eventually ball up and sink \
        </example> \
        </translation> \
        <translation grammarform='noun' transcription='bɔːl' score='0'> \
        <text> \
        a spherical object or mass of material \
        </text> \
        <example> \
        a ball of wool \
        </example> \
        </translation> \
        </translations> \
        </word> \
        </words> \
        ''')
        >>> words = WordContainer()
        >>> ok, msg = words.loadFromFile("Test IO", fh_test)
        >>> print(ok)
        True
        >>> words[0][0].transcription == words[0][1].transcription
        True
        >>> fh_test = StringIO('''<?xml version='1.0' encoding='UTF-8'?> \
        <!DOCTYPE WORDS> \
        <words version='12' groupname=''> \
        <word title='process'> \
        <translations> \
        <translation grammarform='noun' transcription='ˈprəuses' score='0'> \
        <text> \
        a natural series of changes \
        </text> \
        <example> \
        the ageing process \
        </example> \
        </translation> \
        <translation grammarform='verb' transcription='prəˈses' score='0'> \
        <text> \
        perform a series of mechanical or chemical operations on (something) in order to change or preserve it \
        </text> \
        <example> \
        the salmon is quickly processed after harvest to preserve the flavour \
        </example> \
        </translation> \
        </translations> \
        </word> \
        </words> \
        ''')
        >>> ok, msg = words.loadFromFile("Test IO", fh_test)
        >>> print(ok)
        True
        >>> words[0][0].transcription == words[0][1].transcription
        False
        """
        if not translation.grammarForm in WordData.GrammarForms:
           raise TranslationException, "Word {0}: Unknown Grammar Form '{1}' for translation '{2}'".format(
                self.title, translation.grammarForm, translation.text)
        key = "".join((translation.grammarForm, translation.text))
        for pair in self.translationsWithKey:
            if key == pair[0]:
                raise WordDataException, u"Translation '{0}' has been already added!".format(unicode(translation))
        if translation.transcription:
            for pair in self.translationsWithKey:
                if not pair[1].transcription:
                    pair[1].transcription = translation.transcription
        else:
            translation.transcription = self.generalTranscription()
        bisect.insort_left(self.translationsWithKey, [key, translation])
        self.translationFromId[id(translation)] = translation
        self.dirty = True
        return self
    
    
    def deleteTranslation(self, translation):
        if not id(translation) in self.translationFromId:
            return False
        key = "".join((translation.grammarForm, translation.text))
        i = bisect.bisect_left(self.translationsWithKey, [key, translation])
        del self.translationsWithKey[i]
        del self.translationFromId[id(translation)]
        self.dirty = True
    
    
    def updateTranslation(self, translation, grammarForm, transcription, score, text, example):
        if not id(translation) in self.translationFromId:
            return False
        
        key = "".join((translation.grammarForm, translation.text))
        new_key = "".join((grammarForm, text))
        for pair in self.translationsWithKey:
            if pair[0] != key and pair[0] == new_key:
                raise TranslationException, u"Translation '{0}:{1}' has been already added!".format(
                        unicode(grammarForm), unicode(text))
        
        if not translation.transcription:
            translation.transcription = self.generalTranscription()
        i = bisect.bisect_left(self.translationsWithKey, [key, translation])
        self.translationsWithKey[i][0] = new_key
        translation.grammarForm = grammarForm
        translation.transcription = transcription
        translation.score = score
        translation.text = text
        translation.example = example
        self.translationsWithKey.sort()
        
        self.dirty = True
    
    
    def generalTranscription(self):
        transcription = ""
        for translation in self.translations():
            if translation.transcription:
                if transcription and translation.transcription != transcription:
                    # We can't choose the right choise from several options
                    return ""
                transcription = translation.transcription
        return transcription
    
    
    def translations(self):
        for pair in self.translationsWithKey:
            yield pair[1]
    
    
    def copyWithOneTranslation(self, under_scored=True):
        word = WordData(self.title)
        total = [0]
        if len(self) > 1:
            total = range(len(self))
            random.shuffle(total)
        
        if not under_scored:
            translation = self.translationsWithKey[0][1]
            word.addTranslation(translation)
            word.dirty = False
            return word
            
        for pos in total:
            translation = self.translationsWithKey[pos][1]
            if translation.score < 100:
                word.addTranslation(translation)
                word.dirty = False
                return word
        
        raise WordDataException, ("WordData.copyWithOneTranslation: "
                "Not found any translations for word: '{0}', "
                "with under_scored: '{1}'.".format(
                self.title, under_scored))
    
    
    def score(self):
        """ Returns total score of the all translations
        """
        amount_translations = len(self)
        if amount_translations == 0:
            return 0
        result = 0
        for pair in self.translationsWithKey:
            result += pair[1].score
        return result/amount_translations
    
    
    def copyFrom(self, other):
        self.title = other.title
        self.translationsWithKey = []
        self.translationFromId = {}
        for pair in other.translationsWithKey:
            translation = copy.deepcopy(pair[1])
            self.addTranslation(translation)
        self.dirty = other.dirty
    
    
    def __iter__(self):
        for pair in self.translationsWithKey:
            yield pair[1]
    
    
    def __len__(self):
        return len(self.translationsWithKey)
    
    
    def __not__(self):
        return bool(len(self.translationsWithKey))
    
    
    def __getitem__(self, k):
        translation = self.translationsWithKey[k][1]
        return translation
    
    
    def __eq__(self, other):
        if self.title != other.title:
            return False
        if len(self) != len(other):
            return False
        for i in range(len(self)):
            t1 = self[i]
            t2 = other[i]
            if not self[i] == other[i]:
                return False
        return True


class WordContainer(object):
    """ The main data container
    """
    
    OLDEST_FILE_VERSION = 11
    OLDER_FILE_VERSION = 12
    FILE_VERSION_WO_WORD_AMOUNTS=13
    FILE_VERSION = 15
    AMOUNT_REPEATITION_TO_LEARN = 5
    AMOUNT_WORDS_IN_LESSON = 20

    
    def __init__(self):
        """ self.group_name is just a symbolic name. It's one for a file
        """
        self.group_name = ""
        self.repeat_word = WordContainer.AMOUNT_REPEATITION_TO_LEARN
        self.word_amount_exam = WordContainer.AMOUNT_WORDS_IN_LESSON
        self.wordsWithKey = []
        self.wordFromId = {}
        self.dirty = False
    
    
    def isDirty(self):
        if self.dirty:
            return True
        else:
            for pair in self.wordsWithKey:
                if pair[1].dirty:
                    return True
        return False
    
    
    def copyFrom(self, other):
        self.group_name = other.group_name
        self.wordsWithKey = []
        self.wordFromId = {}
        for pair in other.wordsWithKey:
            word = WordData()
            word.copyFrom(pair[1])
            self.addWord(word)
        self.dirty = other.dirty
    
    
    def __len__(self):
        return len(self.wordsWithKey)
    
    
    def __iter__(self):
        for pair in self.wordsWithKey:
            yield pair[1]
    
    
    def __getitem__(self, k):
        word = self.wordsWithKey[k][1]
        return word
    
    
    def addWord(self, word):
        key = word.title
        for pair in self.wordsWithKey:
            if pair[0] == key:
                raise WordContainerException, u"Word '{0}' has been already added!".format(unicode(word.title))
        bisect.insort_left(self.wordsWithKey, [key, word])
        self.wordFromId[id(word)] = word
        self.dirty = True
    
    
    def editWord(self, word, newWord):
        if not id(word) in self.wordFromId:
            return False

        new_key = newWord.title
        key = word.title
        for pair in self.wordsWithKey:
            if pair[0] != key and pair[0] == new_key:
                raise WordContainerException, u"Word '{0}' has been already added!".format(unicode(newWord.title))

        i = bisect.bisect_left(self.wordsWithKey, [key, word])
        self.wordsWithKey[i][0] = new_key
        word.copyFrom(newWord)
        self.wordsWithKey.sort()
        
        self.dirty = True
    
    
    def deleteWord(self, word):
        if not id(word) in self.wordFromId:
            return False
        key = word.title
        i = bisect.bisect_left(self.wordsWithKey, [key, word])
        del self.wordsWithKey[i]
        del self.wordFromId[id(word)]
        self.dirty = True
        return True
    
    
    def loadFromFile(self, filename, fh_test=None):
        """
        >>> fname = "/tmp/123.xml"
        >>> word = WordData("ball")
        >>> translation = Translation("noun", "a spherical object or mass of material", "a ball of wool", u"bɔːl")
        >>> word.addTranslation(translation)
        >>> translation = Translation("verb", "form a round shape", " the fishing nets eventually ball up and sink")
        >>> word.addTranslation(translation)
        >>> words = WordContainer()
        >>> words.addWord(word)
        >>> ok, msg = words.saveToFile(fname)
        >>> print(ok)
        True
        >>> words2 = WordContainer()
        >>> ok, msg = words2.loadFromFile(fname)
        >>> print(ok)
        True
        >>> words[0] == words2[0]
        True
        >>> os.remove(fname)
        >>> words2.clear()
        >>> len(words2)
        0
        >>> fh_test = StringIO('''<?xml version='1.0' encoding='UTF-8'?> \
        <!DOCTYPE WORDS> \
        <words version='12' groupname=''> \
        <word title='ball'> \
        <translations> \
        <translation grammarform='noun' transcription='bɔːl' score='0'> \
        <text> \
        a spherical object or mass of material \
        </text> \
        <example> \
        a ball of wool \
        </example> \
        </translation> \
        <translation grammarform='verb' transcription='' score='0'> \
        <text> \
        form a round shape \
        </text> \
        <example> \
         the fishing nets eventually ball up and sink \
        </example> \
        </translation> \
        </translations> \
        </word> \
        </words> \
        ''')
        >>> ok, msg = words.loadFromFile("Test IO", fh_test)
        >>> print(ok)
        True
        >>> fh_test = StringIO('''<?xml version='1.0' encoding='UTF-8'?> \
        <!DOCTYPE WORDS> \
        <words version='12' groupname=''> \
        <word title='ball'> \
        <translations> \
        <translation grammarform='noun' transcription='bɔːl' score='0'> \
        <text> \
        a spherical object or mass of material \
        </text> \
        <example> \
        </example> \
        </translation> \
        </translations> \
        </word> \
        </words> \
        ''')
        >>> len(words)
        1
        >>> ok, msg = words.loadFromFile("Test IO", fh_test)
        >>> print(msg)
        Failed to load 'Test IO': 'example' of the translation should not be empty!
        >>> words.clear()
        >>> len(words)
        0
        >>> fh_test = StringIO('''<?xml version='1.0' encoding='UTF-8'?> \
        <!DOCTYPE WORDS> \
        <words version='12' groupname=''> \
        <word title='ball'> \
        <translations> \
        <translation grammarform='noun' transcription='bɔːl' score='0'> \
        <text> \
        a spherical object or mass of material \
        </text> \
        </translation> \
        </translations> \
        </word> \
        </words> \
        ''')
        >>> ok, msg = words.loadFromFile("Test IO", fh_test)
        >>> print(msg)
        Failed to load 'Test IO': 'example' of the translation should not be empty!
        >>> fh_test = StringIO('''<?xml version='1.0' encoding='UTF-8'?> \
        <!DOCTYPE WORDS> \
        <words version='12' groupname=''> \
        <word title='ball'> \
        <translations> \
        <translation grammarform='noun' transcription='bɔːl' score='0'> \
        <text> \
        a spherical object or mass of material \
        </text> \
        <example> \
        </translation> \
        </translations> \
        </word> \
        </words> \
        ''')
        >>> ok, msg = words.loadFromFile("Test IO", fh_test)
        >>> print(msg)
        Failed to load 'Test IO': Opening and ending tag mismatch: example line 1 and translation, line 1, column 304
        """
        # TODO: add unit tests
        self.clear()
        error = None
        fh = None
        try:
            if fh_test is not None:
                fh = fh_test
            else:
                fh = codecs.open(filename, "r", CODEC)
            tree = etree.parse(fh)
            # Ugly bug with buffering doctype so it doesnt't changed with replcacement in the file. Pass for a while
            if tree.docinfo.doctype.upper() != "<!DOCTYPE WORDS>":
                raise ValueError, u"'{0}' is not a Words XML file!".format(os.path.basename(filename))
            words = tree.getroot()
            try:
                self.group_name = unicode(words.attrib["groupname"])
                version = int(words.attrib["version"])
                if version <  WordContainer.OLDEST_FILE_VERSION:
                    raise IOError, "Old and unreadable file format '{0}'".format(version)
                elif version > WordContainer.FILE_VERSION:
                    raise IOError, "New and unreadable file format '{0}'. Please update your tool.".format(version)
                if version > WordContainer.FILE_VERSION_WO_WORD_AMOUNTS:
                    self.repeat_word = int(unicode(words.attrib["repeatword"]))
                    self.word_amount_exam = int(unicode(words.attrib["wordamountexam"]))
            except KeyError, e:
                raise ValueError,  u"Not found main attribute: '{0}'".format(unicode(e))
            for wordElement in words:
                word = WordData(decodedSpecialSymbols(wordElement.attrib["title"]))
                if version == WordContainer.OLDEST_FILE_VERSION:
                    for translations in wordElement:
                        for translationElement in translations:
                            grammarForm = translationElement.attrib["grammarform"]
                            for element in translationElement:
                                if element.tag == "text":
                                    text = decodedSpecialSymbols(element.text).strip()
                                elif element.tag == "example":
                                    example = decodedSpecialSymbols(element.text).strip()
                                else:
                                    raise IOError, "Unknown tag: '{0}'".format(element.name)
                            translation = Translation(grammarForm, text, example)
                            word.addTranslation(translation)
                elif version == WordContainer.OLDER_FILE_VERSION:
                    for translations in wordElement:
                        for translationElement in translations:
                            grammarForm = translationElement.attrib["grammarform"]
                            transcription = decodedSpecialSymbols(translationElement.attrib["transcription"])
                            score = int(translationElement.attrib["score"])
                            text = ""
                            example = ""
                            for element in translationElement:
                                if element.tag == "text":
                                    text = decodedSpecialSymbols(element.text).strip()
                                elif element.tag == "example":
                                    example = decodedSpecialSymbols(element.text).strip()
                                else:
                                    raise IOError, "Unknown tag: '{0}'".format(element.name)
                            translation = Translation(grammarForm, text, example, transcription, score)
                            word.addTranslation(translation)
                elif version > WordContainer.OLDER_FILE_VERSION or version == WordContainer.FILE_VERSION:
                    for translations in wordElement:
                        for translationElement in translations:
                            grammarForm = translationElement.attrib["grammarform"]
                            transcription = decodedSpecialSymbols(translationElement.attrib["transcription"])
                            score = int(translationElement.attrib["score"])
                            faults = int(translationElement.attrib["faults"])
                            text = ""
                            example = ""
                            for element in translationElement:
                                if element.tag == "text":
                                    text = decodedSpecialSymbols(element.text).strip()
                                elif element.tag == "example":
                                    example = decodedSpecialSymbols(element.text).strip()
                                else:
                                    raise IOError, "Unknown tag: '{0}'".format(element.name)
                            translation = Translation(grammarForm, text, example, transcription, score, faults)
                            word.addTranslation(translation)
                self.addWord(word)
        except KeyError, e:
            error = u"Attribute '{0}' not found among one of the words".format(unicode(e))
        except TranslationException, e:
            error = u"In the word '{0}': {1}".format(word.title, unicode(e))
        except (OSError, IOError, etree.XMLSyntaxError, ValueError, TranslationException), e:
            error = unicode(e)
        #except:
        #    error = "Unknown and so unhandled exception"
        finally:
            if fh is not None:
                fh.close()
            if error is not None:
                self.clear()
                return False, u"Failed to load '{0}': {1}".format(unicode(filename), unicode(error))
        self.dirty = False
        for pair in self.wordsWithKey:
            pair[1].dirty = False
        return True, "Successfully loaded"
    
    
    def saveToFile(self, filename):
        error = None
        fh = None
        try:
            template_file = os.path.join(os.path.dirname(os.path.realpath(__file__)), WORDDATA_TEMPLATE)
            if not os.path.isfile(template_file):
                    raise ValueError, "Template file '{0}' not found!".format(template_file)
            mako_template = Template(filename=template_file)
            
            fh = codecs.open(filename, "w", CODEC)
            
            fh.write(mako_template.render_unicode(
                    version=WordContainer.FILE_VERSION,
                    container=self))
        except (OSError, IOError, ValueError, mako.exceptions.SyntaxException), e:
            error = unicode(e)
        finally:
            if fh is not None:
                fh.close()
            if error is not None:
                return False, error
        self.dirty = False
        for pair in self.wordsWithKey:
            pair[1].dirty = False
        return True, "Successfully saved"
    
    
    def clear(self):
        self.group_name = ""
        self.repeat_word = WordContainer.AMOUNT_REPEATITION_TO_LEARN
        self.word_amount_exam = WordContainer.AMOUNT_WORDS_IN_LESSON
        self.wordsWithKey = []
        self.wordFromId = {}
    
    
    def __nonzero__(self):
        return bool(self.wordsWithKey)
    
    
    def popRandom(self):
        amount = len(self.wordsWithKey)
        if amount > 1:
            i = random.randrange(1, len(self.wordsWithKey))-1
        else:
            i = 0
        word = self.wordsWithKey[i][1]
        del self.wordsWithKey[i]
        del self.wordFromId[id(word)]
        return word
    
    
    def genUnderScored(self, amount):
        total = range(len(self))
        random.shuffle(total)
        for pos, word_index in enumerate((i for i in total if self.wordsWithKey[i][1].score() < 100)):
            if pos > amount-1:
                raise StopIteration
            word = self.wordsWithKey[word_index][1]
            if len(self) > amount:
                remains = amount - pos
            else:
                remains = len(self) - pos
            yield pos, word, remains
    
    
    def all_translations(self):
        # TODO: change name of the method to the name convention
        for pair in self.wordsWithKey:
            for translation in pair[1]:
                yield translation
    
    
    def allTranslationsWithTitles(self):
        for pair in self.wordsWithKey:
            for translation in pair[1]:
                yield pair[1].title, translation
    
    
    def amountToLearn(self):
        amount = 0
        for pair in self.wordsWithKey:
            if pair[1].score() < 100:
                amount += 1
        return amount
    
    
    def clearAllScores(self):
        for translation in self.all_translations():
            translation.score = 0
            translation.faults = 0
        self.dirty = True
    
    
    def updateGroupName(self, group_name):
        self.group_name = unicode(group_name).strip()
        self.dirty = True
    
    
    def updateRepeatWord(self, repeat):
        if self.repeat_word != repeat:
            self.repeat_word = repeat
            self.dirty = True
    
    
    def updateWordAmountExam(self, amount):
        if self.word_amount_exam != amount:
            self.word_amount_exam = amount
            self.dirty = True
    
    
    def exportExcel(self, fname):
        try:
            book = xlwt.Workbook(encoding=CODEC)
        except NameError:
            # Moved xlwt here because it is quiet hard to import and would reduce speed of the start program a bit
            import xlwt
            book = xlwt.Workbook(encoding=CODEC)
        sheet = book.add_sheet("Sheet 1", cell_overwrite_ok=True)
        caption_style = xlwt.easyxf('borders: left thin, right thin, top thin, bottom thin;'
                    'alignment: horizontal left, vertical centre, wrap true;'
                    'font: bold true, italic true;')
        style = xlwt.easyxf('borders: left thin, right thin, top thin, bottom thin;'
                    'alignment: horizontal left, vertical centre, wrap true;')
        sheet.write(0, 0, "Title", caption_style)
        sheet.write(0, 1, "Transcription", caption_style)
        sheet.write(0, 2, "Grammar Form", caption_style)
        sheet.write(0, 3, "Text", caption_style)
        sheet.write(0, 4, "Example", caption_style)
        previous_title = ""
        for i, pair in enumerate(self.allTranslationsWithTitles(), 1):
            title, translation = pair
            if previous_title == title:
                sheet.write_merge(i-1, i, 0, 0, title, style)
            else:
                sheet.write(i, 0, title, style)
            sheet.write(i, 1, u"[{0}]".format(translation.transcription) if translation.transcription else "", style)
            sheet.write(i, 2, translation.grammarForm, style)
            sheet.write(i, 3, translation.text, style)
            sheet.write(i, 4, translation.example, style)
            previous_title = title[:]
        try:
            book.save(fname)
        except IOError, e:
            return False, unicode(e)
        return True, "Ok"
    
    def encode(self, text):
        return text.replace("\n\n", NEWPARA).replace("\n", NEWLINE).replace("'", APOSTROPHE).replace("<", LEFTANGELBRACKET).replace(">", RIGHTANGELBRACKET)


if __name__ == "__main__":
    from StringIO import StringIO
    import doctest
    doctest.testmod()
