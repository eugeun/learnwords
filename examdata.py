#!/usr/bin/env python2.7
"""This file provides two main classess: Respond and Examination.
Examination class takes worddata.WordContainer() at initializaion and provides methods such as: check() and hint().
Respond class is what the Examination.check(userAnswer) returns. It holds the result and also flag of the end.
"""
import time
import bisect


from worddata import WordContainer, WordData, WordDataException


class ExaminationException(Exception): pass


class Task(WordData): pass


class Hint(WordData): pass


class FullAnswer(WordData): pass


class ExamDirectionData(object):
    
    def __init__(self, caption, shortcut):
        self.caption = caption
        self.name = caption.strip("&").lower()
        self.icon = "examorder{0}".format(self.name)
        self.tip = "Set {0} examination direction".format(self.name)
        self.shortcut = shortcut


class ExamDirectionContainer(object):
    
    def __init__(self):
        self.directions = []
        self.directionFromId = {}
    
    
    def add(self, direction):
        key = direction.name
        bisect.insort_left(self.directions, [key, direction])
        self.directionFromId[id(direction)] = direction
    
    
    def __iter__(self):
        for pair in self.directions:
            yield pair[1]
    
    
    def __contains__(self, name):
        target = name.lower()
        for pair in self.directions:
            if target == pair[1].name:
                return True
        return False
    

class ExaminationData(object):
    
    ExamDirections=ExamDirectionContainer()
    for caption, shortcut in (("&Forward", "Ctrl+F"),
        ("&Backward", "Ctrl+B"),
        ("&Mixed", "Ctrl+M")):
        ExamDirections.add(ExamDirectionData(caption, shortcut))
    
    def __init__(self):
        self.task_generator = None
        self.taskWord = None
        self.taskAnswer = None
        self.task_number = 0
        self.remains = 0
        self.learn_value = 0
        self.order = None
        self.current_order = None
        self.hintedEntity = None
    
    
    def start(self, words, direction_id):
        try:
            direction = ExaminationData.ExamDirections.directionFromId[direction_id]
        except KeyError:
            raise ExaminationException, "Unknown exam direction object id: '{0}'".format(direction_id)
        self.order = direction.name
        self.learn_value = int(1.0/words.repeat_word*100)
        self.hintedEntity = None
        self.task_generator = words.genUnderScored(words.word_amount_exam)
    
    
    def next(self):
        try:
            self.task_number, self.taskWord, self.remains = self.task_generator.next()
        except StopIteration:
            self.clear()
            raise StopIteration
        self.taskAnswer = self.taskWord.copyWithOneTranslation()
        task = WordData()
        task.copyFrom(self.taskAnswer)
        if self.order != "mixed":
            self.current_order = self.order
        else:
            #orders = ["forward", "backward"]
            #i = int(time.time())%2
            #self.current_order = orders[i]
            # may be direct algorithm would be better for a while
            if self.current_order == "forward":
                self.current_order = "backward"
            else:
                self.current_order = "forward"
        if self.current_order == "forward":
            translation = task.translationsWithKey[0][1]
            translation.text = ""
        elif self.current_order == "backward":
            task.title = ""
            translation = task.translationsWithKey[0][1]
            translation.transcription = ""
            translation.grammarForm = ""
            translation.example = ""
        return task
    
    
    def check(self, choice):
        correct = False
        answerTranslation = self.taskAnswer.translationsWithKey[0][1]
        originalTranslation = None
        for translation in self.taskWord:
            if translation == answerTranslation:
                originalTranslation = translation
                break
        
        if choice.title:
            if choice.title.lower() == self.taskAnswer.title.lower():
                correct = True
        else:
            choiceTranslation = choice.translationsWithKey[0][1]
            if answerTranslation == choiceTranslation:
                correct = True
        if correct:
            originalTranslation.addScore(self.learn_value)
        else:
            originalTranslation.subScore(self.learn_value)
        self.taskWord.dirty = True
        return correct
    
    
    def hint(self):
        answerTranslation = self.taskAnswer.translationsWithKey[0][1]
        originalTranslation = None
        for translation in self.taskWord:
            if translation == answerTranslation:
                originalTranslation = translation
                break
        originalTranslation.subScore(self.learn_value)
        self.taskWord.dirty = True
        # It can be run twice on the same ExamIssue, in the second time respond will hole the full answer
        return self.fullAnswer()
    
    
    def fullAnswer(self):
        respond = FullAnswer()
        respond.copyFrom(self.taskAnswer)
        return respond
    
    
    def isRunning(self):
        return bool(self.task_generator is not None)
    
    
    def clear(self):
        self.__init__()
