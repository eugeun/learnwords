# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '/home/epomnikov/Projects/LearnWords/ui/addedittranslationdlg.ui'
#
# Created: Thu Feb 14 08:59:06 2013
#      by: PyQt4 UI code generator 4.9.1
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    _fromUtf8 = lambda s: s

class Ui_AddEditTranslationDlg(object):
    def setupUi(self, AddEditTranslationDlg):
        AddEditTranslationDlg.setObjectName(_fromUtf8("AddEditTranslationDlg"))
        AddEditTranslationDlg.resize(418, 278)
        AddEditTranslationDlg.setStyleSheet(_fromUtf8(""))
        self.gridLayout = QtGui.QGridLayout(AddEditTranslationDlg)
        self.gridLayout.setObjectName(_fromUtf8("gridLayout"))
        self.grammarFormScoreLayout = QtGui.QHBoxLayout()
        self.grammarFormScoreLayout.setObjectName(_fromUtf8("grammarFormScoreLayout"))
        self.label_2 = QtGui.QLabel(AddEditTranslationDlg)
        self.label_2.setObjectName(_fromUtf8("label_2"))
        self.grammarFormScoreLayout.addWidget(self.label_2)
        self.grammarFormComboBox = QtGui.QComboBox(AddEditTranslationDlg)
        self.grammarFormComboBox.setObjectName(_fromUtf8("grammarFormComboBox"))
        self.grammarFormScoreLayout.addWidget(self.grammarFormComboBox)
        spacerItem = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.grammarFormScoreLayout.addItem(spacerItem)
        self.label_8 = QtGui.QLabel(AddEditTranslationDlg)
        self.label_8.setObjectName(_fromUtf8("label_8"))
        self.grammarFormScoreLayout.addWidget(self.label_8)
        self.scoreSpinBox = QtGui.QSpinBox(AddEditTranslationDlg)
        self.scoreSpinBox.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.scoreSpinBox.setMaximum(100)
        self.scoreSpinBox.setObjectName(_fromUtf8("scoreSpinBox"))
        self.grammarFormScoreLayout.addWidget(self.scoreSpinBox)
        self.gridLayout.addLayout(self.grammarFormScoreLayout, 0, 0, 1, 1)
        self.buttonLayout = QtGui.QVBoxLayout()
        self.buttonLayout.setObjectName(_fromUtf8("buttonLayout"))
        self.saveButton = QtGui.QPushButton(AddEditTranslationDlg)
        self.saveButton.setEnabled(False)
        self.saveButton.setObjectName(_fromUtf8("saveButton"))
        self.buttonLayout.addWidget(self.saveButton)
        self.closeButton = QtGui.QPushButton(AddEditTranslationDlg)
        self.closeButton.setObjectName(_fromUtf8("closeButton"))
        self.buttonLayout.addWidget(self.closeButton)
        spacerItem1 = QtGui.QSpacerItem(20, 40, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.buttonLayout.addItem(spacerItem1)
        self.gridLayout.addLayout(self.buttonLayout, 0, 1, 3, 1)
        self.transcriptionLayout = QtGui.QHBoxLayout()
        self.transcriptionLayout.setObjectName(_fromUtf8("transcriptionLayout"))
        self.label_7 = QtGui.QLabel(AddEditTranslationDlg)
        self.label_7.setObjectName(_fromUtf8("label_7"))
        self.transcriptionLayout.addWidget(self.label_7)
        self.transcriptionEdit = QtGui.QLineEdit(AddEditTranslationDlg)
        self.transcriptionEdit.setObjectName(_fromUtf8("transcriptionEdit"))
        self.transcriptionLayout.addWidget(self.transcriptionEdit)
        self.gridLayout.addLayout(self.transcriptionLayout, 1, 0, 1, 1)
        self.translationTextEdit = QtGui.QTextEdit(AddEditTranslationDlg)
        self.translationTextEdit.setObjectName(_fromUtf8("translationTextEdit"))
        self.gridLayout.addWidget(self.translationTextEdit, 2, 0, 1, 1)
        self.exampleLabelLayout = QtGui.QHBoxLayout()
        self.exampleLabelLayout.setObjectName(_fromUtf8("exampleLabelLayout"))
        self.label_3 = QtGui.QLabel(AddEditTranslationDlg)
        self.label_3.setObjectName(_fromUtf8("label_3"))
        self.exampleLabelLayout.addWidget(self.label_3)
        spacerItem2 = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.exampleLabelLayout.addItem(spacerItem2)
        self.gridLayout.addLayout(self.exampleLabelLayout, 3, 0, 1, 2)
        self.exampleTextEdit = QtGui.QTextEdit(AddEditTranslationDlg)
        self.exampleTextEdit.setObjectName(_fromUtf8("exampleTextEdit"))
        self.gridLayout.addWidget(self.exampleTextEdit, 4, 0, 1, 2)
        self.label_2.setBuddy(self.grammarFormComboBox)
        self.label_8.setBuddy(self.scoreSpinBox)
        self.label_7.setBuddy(self.transcriptionEdit)
        self.label_3.setBuddy(self.exampleTextEdit)

        self.retranslateUi(AddEditTranslationDlg)
        QtCore.QObject.connect(self.closeButton, QtCore.SIGNAL(_fromUtf8("clicked()")), AddEditTranslationDlg.reject)
        QtCore.QObject.connect(self.saveButton, QtCore.SIGNAL(_fromUtf8("clicked()")), AddEditTranslationDlg.accept)
        QtCore.QMetaObject.connectSlotsByName(AddEditTranslationDlg)
        AddEditTranslationDlg.setTabOrder(self.grammarFormComboBox, self.scoreSpinBox)
        AddEditTranslationDlg.setTabOrder(self.scoreSpinBox, self.transcriptionEdit)
        AddEditTranslationDlg.setTabOrder(self.transcriptionEdit, self.translationTextEdit)
        AddEditTranslationDlg.setTabOrder(self.translationTextEdit, self.exampleTextEdit)
        AddEditTranslationDlg.setTabOrder(self.exampleTextEdit, self.saveButton)
        AddEditTranslationDlg.setTabOrder(self.saveButton, self.closeButton)

    def retranslateUi(self, AddEditTranslationDlg):
        AddEditTranslationDlg.setWindowTitle(QtGui.QApplication.translate("AddEditTranslationDlg", "Add Word", None, QtGui.QApplication.UnicodeUTF8))
        self.label_2.setText(QtGui.QApplication.translate("AddEditTranslationDlg", "&Grammar form:", None, QtGui.QApplication.UnicodeUTF8))
        self.label_8.setText(QtGui.QApplication.translate("AddEditTranslationDlg", "&Score:", None, QtGui.QApplication.UnicodeUTF8))
        self.saveButton.setText(QtGui.QApplication.translate("AddEditTranslationDlg", "&Add", None, QtGui.QApplication.UnicodeUTF8))
        self.closeButton.setText(QtGui.QApplication.translate("AddEditTranslationDlg", "&Close", None, QtGui.QApplication.UnicodeUTF8))
        self.label_7.setText(QtGui.QApplication.translate("AddEditTranslationDlg", "&Transcription:", None, QtGui.QApplication.UnicodeUTF8))
        self.translationTextEdit.setToolTip(QtGui.QApplication.translate("AddEditTranslationDlg", "<html><head/><body><p>Type translation here</p></body></html>", None, QtGui.QApplication.UnicodeUTF8))
        self.label_3.setText(QtGui.QApplication.translate("AddEditTranslationDlg", "&Example:", None, QtGui.QApplication.UnicodeUTF8))
        self.exampleTextEdit.setToolTip(QtGui.QApplication.translate("AddEditTranslationDlg", "<html><head/><body><p>Type example of the use here</p></body></html>", None, QtGui.QApplication.UnicodeUTF8))


if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
    AddEditTranslationDlg = QtGui.QDialog()
    ui = Ui_AddEditTranslationDlg()
    ui.setupUi(AddEditTranslationDlg)
    AddEditTranslationDlg.show()
    sys.exit(app.exec_())

