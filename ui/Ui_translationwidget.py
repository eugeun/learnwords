# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'C:\Users\Naddy_000\Projects\LearnWords\ui\translationwidget.ui'
#
# Created: Mon Sep 16 01:13:31 2013
#      by: PyQt4 UI code generator 4.9.6
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_TranslationWidget(object):
    def setupUi(self, TranslationWidget):
        TranslationWidget.setObjectName(_fromUtf8("TranslationWidget"))
        TranslationWidget.resize(973, 752)
        self.verticalLayout = QtGui.QVBoxLayout(TranslationWidget)
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
        self.gridLayout = QtGui.QGridLayout()
        self.gridLayout.setObjectName(_fromUtf8("gridLayout"))
        self.horizontalLayout_2 = QtGui.QHBoxLayout()
        self.horizontalLayout_2.setObjectName(_fromUtf8("horizontalLayout_2"))
        self.label_3 = QtGui.QLabel(TranslationWidget)
        self.label_3.setObjectName(_fromUtf8("label_3"))
        self.horizontalLayout_2.addWidget(self.label_3)
        self.titleEdit = QtGui.QLineEdit(TranslationWidget)
        self.titleEdit.setEnabled(False)
        self.titleEdit.setObjectName(_fromUtf8("titleEdit"))
        self.horizontalLayout_2.addWidget(self.titleEdit)
        self.gridLayout.addLayout(self.horizontalLayout_2, 0, 0, 1, 1)
        self.horizontalLayout_4 = QtGui.QHBoxLayout()
        self.horizontalLayout_4.setObjectName(_fromUtf8("horizontalLayout_4"))
        self.label = QtGui.QLabel(TranslationWidget)
        self.label.setObjectName(_fromUtf8("label"))
        self.horizontalLayout_4.addWidget(self.label)
        self.transcriptionEdit = QtGui.QLineEdit(TranslationWidget)
        self.transcriptionEdit.setEnabled(False)
        self.transcriptionEdit.setObjectName(_fromUtf8("transcriptionEdit"))
        self.horizontalLayout_4.addWidget(self.transcriptionEdit)
        self.gridLayout.addLayout(self.horizontalLayout_4, 0, 1, 1, 1)
        self.horizontalLayout_5 = QtGui.QHBoxLayout()
        self.horizontalLayout_5.setObjectName(_fromUtf8("horizontalLayout_5"))
        self.label_2 = QtGui.QLabel(TranslationWidget)
        self.label_2.setObjectName(_fromUtf8("label_2"))
        self.horizontalLayout_5.addWidget(self.label_2)
        self.scoreEdit = QtGui.QLineEdit(TranslationWidget)
        self.scoreEdit.setEnabled(False)
        self.scoreEdit.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.scoreEdit.setObjectName(_fromUtf8("scoreEdit"))
        self.horizontalLayout_5.addWidget(self.scoreEdit)
        self.gridLayout.addLayout(self.horizontalLayout_5, 0, 2, 1, 1)
        self.verticalLayout.addLayout(self.gridLayout)
        self.groupBox = QtGui.QGroupBox(TranslationWidget)
        self.groupBox.setObjectName(_fromUtf8("groupBox"))
        self.horizontalLayout = QtGui.QHBoxLayout(self.groupBox)
        self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))
        self.translationText = QtGui.QTextBrowser(self.groupBox)
        self.translationText.setEnabled(False)
        self.translationText.setObjectName(_fromUtf8("translationText"))
        self.horizontalLayout.addWidget(self.translationText)
        self.verticalLayout.addWidget(self.groupBox)
        self.groupBox_2 = QtGui.QGroupBox(TranslationWidget)
        self.groupBox_2.setObjectName(_fromUtf8("groupBox_2"))
        self.horizontalLayout_3 = QtGui.QHBoxLayout(self.groupBox_2)
        self.horizontalLayout_3.setObjectName(_fromUtf8("horizontalLayout_3"))
        self.examplesText = QtGui.QTextBrowser(self.groupBox_2)
        self.examplesText.setEnabled(False)
        self.examplesText.setObjectName(_fromUtf8("examplesText"))
        self.horizontalLayout_3.addWidget(self.examplesText)
        self.verticalLayout.addWidget(self.groupBox_2)

        self.retranslateUi(TranslationWidget)
        QtCore.QMetaObject.connectSlotsByName(TranslationWidget)

    def retranslateUi(self, TranslationWidget):
        TranslationWidget.setWindowTitle(_translate("TranslationWidget", "Form", None))
        self.label_3.setText(_translate("TranslationWidget", "Title:", None))
        self.label.setText(_translate("TranslationWidget", "Transcription:", None))
        self.label_2.setText(_translate("TranslationWidget", "Score:", None))
        self.groupBox.setTitle(_translate("TranslationWidget", "Text", None))
        self.groupBox_2.setTitle(_translate("TranslationWidget", "Examples", None))


if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
    TranslationWidget = QtGui.QWidget()
    ui = Ui_TranslationWidget()
    ui.setupUi(TranslationWidget)
    TranslationWidget.show()
    sys.exit(app.exec_())

