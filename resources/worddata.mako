<?xml version='1.0' encoding='UTF-8'?>
<!DOCTYPE WORDS>
<words version='${version}' groupname='${container.group_name}' repeatword='${container.repeat_word}' wordamountexam='${container.word_amount_exam}'>
% for word in container:
    <word title='${container.encode(word.title)}'>
        <translations>
% for translation in word:
            <translation grammarform='${translation.grammarForm}' transcription='${container.encode(translation.transcription)}' score='${translation.score}' faults='${translation.faults}'>
                <text>
                    ${container.encode(translation.text)}
                </text>
                <example>
                    ${container.encode(translation.example)}
                </example>
            </translation>
% endfor
            </translations>
    </word>
% endfor
</words>
