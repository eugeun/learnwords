#!/usr/bin/env python2.7
""" This application tended to be help in learning words with examples
"""

from PyQt4.QtGui import (QApplication, QMainWindow, QActionGroup, QAction, 
            QIcon, QLabel, QFrame, QTableWidget, QTableWidgetItem, QFileDialog, 
            QMessageBox, QAction, QShortcut, QKeySequence, QSpinBox)
from PyQt4.QtCore import (Qt, SIGNAL, pyqtSignature, QSettings, QTimer, 
            QVariant, QString, QT_VERSION_STR, PYQT_VERSION_STR)
from translationtabs import TranslationTabs
from addeditworddlg import AddEditWordDlg
from addedittranslationdlg import AddEditTranslationDlg
from ui import Ui_mainwindow
import sys
import os
import platform
from worddata import WordData, WordContainer, WordDataException, Translation
from examdata import ExaminationData, ExaminationException, FullAnswer


__version__ = '1.0 pre-release'


class MainWindow(QMainWindow, Ui_mainwindow.Ui_MainWindow):
    
    def __init__(self, parent=None):
        """ Keep in mind: statustips of all actions were added in the file *.ui through text editor (not Designer)!
        """
        super(MainWindow, self).__init__(parent)
        self.setupUi(self)
        
        self.repeatWordSpinBox = QSpinBox()
        self.repeatWordSpinBox.setRange(2, 99)
        self.repeatWordSpinBox.setValue(2)
        self.repeatWordSpinBox.setSuffix(" Times")
        self.repeatWordSpinBox.setToolTip("Repeat a word in exam to learn")
        self.repeatWordSpinBox.setStatusTip(self.repeatWordSpinBox.toolTip())
        self.repeatWordSpinBox.setFocusPolicy(Qt.NoFocus)
        self.editToolBar.addWidget(self.repeatWordSpinBox)
        
        self.wordAmountExamSpinBox = QSpinBox()
        self.wordAmountExamSpinBox.setRange(2, 99)
        self.wordAmountExamSpinBox.setValue(20)
        self.wordAmountExamSpinBox.setSingleStep(5)
        self.wordAmountExamSpinBox.setSuffix(" Amount")
        self.wordAmountExamSpinBox.setToolTip("Amount of words in one exam")
        self.wordAmountExamSpinBox.setStatusTip(self.wordAmountExamSpinBox.toolTip())
        self.wordAmountExamSpinBox.setFocusPolicy(Qt.NoFocus)
        self.editToolBar.addWidget(self.wordAmountExamSpinBox)
        
        self.editWordTable = QTableWidget()
        self.editTranslationTabs = TranslationTabs()
        self.editPageSplitter.addWidget(self.editWordTable)
        self.editPageSplitter.addWidget(self.editTranslationTabs)
        
        self.filename = ""
        self.last_filename = ""
        self.words = WordContainer()
        self.examination = ExaminationData()
        self.appName = QApplication.applicationName()
        
        self.viewGroup = QActionGroup(self)
        self.addActions(self.viewGroup, (self.viewEditAction, self.viewExamAction))
        self.viewEditAction.setChecked(True)
        
        self.examOrderGroup = QActionGroup(self)
        for direction in self.examination.ExamDirections:
            action = self.createAction(direction.caption, 
                    self.updateExamDirection, 
                    direction.shortcut, 
                    direction.icon, 
                    direction.tip)
            action.setData(QVariant(long(id(direction))))
            self.examOrderGroup.addAction(action)
            self.examToolBar.addAction(action)
        action.setChecked(True)
        self.examToolBar.addSeparator()
        
        self.amountLabel = QLabel()
        self.amountLabel.setFrameStyle(QFrame.StyledPanel|QFrame.Sunken)
        self.statusBar.setSizeGripEnabled(False)
        self.statusBar.addPermanentWidget(self.amountLabel)
        self.statusBar.showMessage("Ready...", 5000)
        
        settings = QSettings()
        self.restoreGeometry(settings.value("MainWindow/Geometry").toByteArray())
        self.restoreState(settings.value("MainWindow/State").toByteArray())
        self.editPageSplitter.restoreState(settings.value("Splitter").toByteArray())

        self.connect(self.editWordTable, 
                SIGNAL("currentItemChanged(QTableWidgetItem*, QTableWidgetItem*)"), 
                self.updateEditTranslationTabs)
        self.connect(self.repeatWordSpinBox, SIGNAL("valueChanged(int)"), self.words.updateRepeatWord)
        self.connect(self.wordAmountExamSpinBox, SIGNAL("valueChanged(int)"), self.words.updateWordAmountExam)
        self.connect(self.viewGroup, SIGNAL("triggered(QAction *)"), self.updateView)
        self.connect(self.examTitleEdit, SIGNAL("returnPressed()"), self.examEnterButton, 
                SIGNAL("clicked()"))
        QShortcut(QKeySequence("Return"), self.editWordTable, self.on_serviceEditWordAction_triggered)
        QShortcut(QKeySequence("Del"), self.editWordTable, self.on_serviceRemoveWordAction_triggered)
        
        self.mainStackedWidget.setCurrentIndex(0)
        self.setWindowTitle(self.appName)
        QTimer.singleShot(0, self.loadInitialFile)
    
    
    def createAction(self, text, slot=None, shortcut=None, 
                                icon=None, tip=None, checkable=True, 
                                signal="toggled(bool)"):
        action = QAction(text, self)
        if icon is not None:
            action.setIcon(QIcon(":/%s.png" % icon))
        if shortcut is not None:
            action.setShortcut(shortcut)
        if tip is not None:
            action.setToolTip(tip)
            action.setStatusTip(tip)
        if slot is not None:
            self.connect(action, SIGNAL(signal), slot)
        if checkable:
            action.setCheckable(True)
        return action
    
    
    def addActions(self, target, actions):
        for action in actions:
            target.addAction(action)
    
    
    def updateExamDirection(self, checked):
        if self.examination.isRunning() and checked:
            QMessageBox.warning(self, 
                    "{0} - Change Exam Direction".format(self.appName),
                    "New exam direction will be applied only after exam restart!")
    
    
    def loadInitialFile(self):
        settings = QSettings()
        filename = unicode(settings.value("LastFile").toString())
        if filename and os.path.exists(filename):
            self.filename = filename
            ok, msg = self.words.loadFromFile(self.filename)
            if not ok:
                QMessageBox.critical(self, "{0} - Open Word Data".format(self.appName), msg)
        self.updateEditView()
    
    
    @pyqtSignature("")
    def on_fileNewAction_triggered(self):
        if not self.okToContinue():
            return
        self.filename = ""
        self.groupNameEdit.clear()
        self.words = WordContainer()
        self.examination.clear()
        if self.mainStackedWidget.currentIndex() == 0:
            self.updateEditView()
        else:
            self.updateExamView()
    
    
    @pyqtSignature("")
    def on_fileOpenAction_triggered(self):
        if not self.okToContinue():
            return
        fname = (self.filename
            if self.filename else self.last_filename)
        fname = QFileDialog.getOpenFileName(self, 
                "{0} - Open Word Data".format(self.appName), fname, 
                "XML files (*.xml)")
        if not fname.isEmpty():
            if not fname.contains("."):
                fname += ".xml"
            self.filename = unicode(fname)
            ok, msg = self.words.loadFromFile(self.filename)
            if not ok:
                QMessageBox.critical(self, "{0} - Open Word Data".format(self.appName), msg)
            else:
                if self.mainStackedWidget.currentIndex() == 0:
                    self.updateEditView()
                else:
                    self.updateExamView()
            return ok
        else:
            return False
    
    
    @pyqtSignature("")
    def on_fileSaveAction_triggered(self):
        if self.okToSave():
            if not self.filename:
                return self.on_fileSaveAsAction_triggered()
            else:
                return self.saveWordsRoutine()
    
    
    @pyqtSignature("")
    def on_fileSaveAsAction_triggered(self):
        if self.okToSave():
            fname = (self.filename 
                if self.filename else os.path.dirname(self.last_filename))
            fname = QFileDialog.getSaveFileName(self, 
                    "{0} - Save Word Data".format(self.appName), fname, 
                    "XML files (*.xml)")
            if fname.isEmpty():
                return False
            
            if not fname.contains("."):
                fname += ".xml"
            self.filename = unicode(fname)
            
            return self.saveWordsRoutine()
    
    
    def saveWordsRoutine(self):
        ok, msg = self.words.saveToFile(self.filename)
        if not ok:
            QMessageBox.critical(self, "{0} - Save Word Data".format(self.appName), msg)
        else:
            self.statusBar.showMessage("Saved to '{0}'...".format(os.path.basename(self.filename)), 5000)
            self.setWindowTitle("{0} - {1} (Edit Mode)".format(
                    self.appName, os.path.basename(self.filename)))            
        return ok
    
    
    @pyqtSignature("")
    def on_fileExportExcelAction_triggered(self):
        if self.okToSave():
            # TODO: replace rfind with "".split as more simple solution
            if not self.filename:
                fname = os.path.dirname(self.last_filename)
            else:
                fname = self.filename[:]
                i = self.filename.rfind(".")
                if i > 0:
                    fname = self.filename[:i]
            fname = QFileDialog.getSaveFileName(self, 
                    "{0} - Export Word Data".format(self.appName), fname, 
                    "Excel files (*.xls)")
            if fname.isEmpty():
                return False
            
            if self.filename and not fname.contains("."):
                # Add extension if  'fname' only is a 'self.filename' copy (not a directory of self.last_filename)
                fname += ".xls"
            
            ok, msg = self.words.exportExcel(unicode(fname))
            if not ok:
                QMessageBox.critical(self, "{0} - Export Word Data".format(self.appName), msg)
            else:
                self.statusBar.showMessage(u"Exported {0} words to the '{1}'...".format(len(self.words), unicode(fname)),  5000)
            return ok
    
    
    @pyqtSignature("")
    def on_serviceAddWordAction_triggered(self):
        form = AddEditWordDlg(self.words)
        if form.exec_():
            self.updateWordTable(id(form.editedWord))


    @pyqtSignature("")
    def on_serviceEditWordAction_triggered(self):
        word = self.currentWord()
        if word is not None:
            form = AddEditWordDlg(self.words, word)
            if form.exec_():
                self.updateWordTable(id(word))
                self.amountLabel.setText("Total {0}: / To Learn: {1}".format(
                        len(self.words), self.words.amountToLearn()))
    
    
    @pyqtSignature("")
    def on_serviceRemoveWordAction_triggered(self):
        word = self.currentWord()
        if word is not None:
            reply = QMessageBox.question(self, 
                    self.appName, 
                    u"Remove word: '{0}'?".format(unicode(word.title)), 
                    QMessageBox.Yes|QMessageBox.No)
            if reply == QMessageBox.Yes:
                if self.words.deleteWord(word):
                    self.updateWordTable()
                    self.statusBar.showMessage("Removed word '{0}'...".format(word.title), 5000)
    
    
    @pyqtSignature("")
    def on_serviceAddTranslationAction_triggered(self):
        QMessageBox.warning(self, 
                "{0} - Add Translation".format(self.appName),
                "This action has not been realized yet!<br/>"
                "Please, add translations through the 'Edit Word' dialog.")
    
    
    @pyqtSignature("")
    def on_serviceEditTranslationAction_triggered(self):
        word = self.currentWord()
        if word is None:
            return
        
        translation = self.currentTranslation(word)
        if translation is None:
            return
        
        form = AddEditTranslationDlg(word, translation)
        if form.exec_():
            self.on_fileSaveAction_triggered()
            self.updateWordTable(id(word), id(translation))
            self.amountLabel.setText("Total {0}: / To Learn: {1}".format(
                    len(self.words), self.words.amountToLearn()))
    
    
    @pyqtSignature("")
    def on_serviceRemoveTranslationAction_triggered(self):
        QMessageBox.warning(self, 
                "{0} - Remove Translation".format(self.appName),
                "This action has not been realized yet!<br/>"
                "Please, remove translations through the 'Edit Word' dialog.")
    
    
    @pyqtSignature("")
    def on_helpAboutAction_triggered(self):
        QMessageBox.about(self, 
                "About {0}".format(self.appName), 
                """<b>{0}</b> v {1} 
                <p>This tool helps to learn words with support:
                <ul><li>separate grammar forms;</li> 
                <li>transcriptions;</li>
                <li>different learn directions (forward, backward, mixed).</li></ul>
                <p>Python {2} - Qt {3} - PyQt {4} on {5}""".format(
                            self.appName, 
                            __version__, 
                            platform.python_version(), 
                            QT_VERSION_STR, 
                            PYQT_VERSION_STR, 
                            platform.system()))
    
    
    @pyqtSignature("QString")
    def on_groupNameEdit_textEdited(self, text):
        # There's something strange with sending textEdited signal to the self.words, 
        # +so I have to use pyqtSignature instead of traditional self.connect. Also spinboxes work well without pyqtSignature
        self.words.updateGroupName(text)
    
    
    @pyqtSignature("QTableWidgetItem*")
    def on_editWordTable_itemDoubleClicked(self, tableItem=None):
        self.on_serviceEditWordAction_triggered()


    def updateWordTable(self, currentWordId=None, currentTranslationId=None):
        self.editWordTable.clear()
        self.editWordTable.setRowCount(len(self.words))
        self.editWordTable.setColumnCount(2)
        self.editWordTable.setHorizontalHeaderLabels(["Title", "Score"])
        self.editWordTable.setAlternatingRowColors(True)
        self.editWordTable.setEditTriggers(QTableWidget.NoEditTriggers)
        self.editWordTable.setSelectionBehavior(QTableWidget.SelectRows)
        self.editWordTable.setSelectionMode(QTableWidget.SingleSelection)        
        selected = None
        
        for row, word in enumerate(self.words):
            item = QTableWidgetItem(word.title)
            if currentWordId is not None and currentWordId == id(word):
                selected = item
            item.setData(Qt.UserRole, QVariant(long(id(word))))
            self.editWordTable.setItem(row, 0, item)
            item = QTableWidgetItem(unicode(word.score()))
            item.setTextAlignment(Qt.AlignRight|Qt.AlignVCenter)
            self.editWordTable.setItem(row, 1, item)
        
        self.editWordTable.resizeColumnsToContents()
        
        if selected is not None:
            selected.setSelected(True)
            self.editWordTable.setCurrentItem(selected)
            self.editWordTable.scrollToItem(selected)
        
        self.amountLabel.setText("Total {0}: / To Learn: {1}".format(len(self.words), self.words.amountToLearn()))
        self.editWordTable.setFocus()
        self.updateEditTranslationTabs(None, None, currentTranslationId)
    
    
    def updateEditTranslationTabs(self, previous=None, current=None, currentTranslationId=None):
        word = self.currentWord()
        if word is None:
            # TODO: really need to do?!
            self.editTranslationTabs.clear()
        else:
            self.editTranslationTabs.populate(word.title, word.translations(), currentTranslationId)

    
    def currentWord(self):
        row = self.editWordTable.currentRow()
        if row > -1:
            item = self.editWordTable.item(row, 0)
            id = item.data(Qt.UserRole).toLongLong()[0]
            return self.words.wordFromId[id]
        else:
            return None
    
    
    def currentTranslation(self, word):
        translation_id = self.editTranslationTabs.currentTranslationId()
        if translation_id is None:
            return None
        try:
            translation = word.translationFromId[translation_id]
        except KeyError:
            return None
        return translation
    
    
    @pyqtSignature("")
    def on_examStartButton_clicked(self):
        if self.examination.isRunning():
            reply = QMessageBox.question(self, 
                    "{0} - Change Mode".format(self.appName), 
                    "Do you want to break the current exam?", 
                    QMessageBox.Yes|QMessageBox.No)
            if reply == QMessageBox.No:
                return

        self.examination.clear()
        
        currentExamDirectionAction = self.examOrderGroup.checkedAction()
        direction_id = currentExamDirectionAction.data().toLongLong()[0]
        
        try:
            self.examination.start(
                    self.words, 
                    direction_id)            
            self.clearExamView()
            task = self.examination.next()
            self.updateExamView(task)
        except StopIteration:
            QMessageBox.critical(self, 
                    "{0} - Empty Exam".format(self.appName),
                    ("You've already learnt all words in the current set. "
                    "Please, downgrade score of the words or open another set."))
            self.examination.clear()
        except (WordDataException, ExaminationException), e:
            QMessageBox.critical(self, 
                "{0} - Exam Starting".format(self.appName),
                unicode(e))
            self.examination.clear()
    
    
    @pyqtSignature("")
    def on_examEnterButton_clicked(self):
        choice = WordData()
        if self.examTitleEdit.isEnabled():
            choice.title = unicode(self.examTitleEdit.text()).strip()
        elif self.examTranslationGroupBox.isEnabled():
            grammar_form = unicode(self.examGrammarFormComboBox.currentText())
            text = unicode(self.examTranslationTextEdit.toPlainText()).strip()
            translation = Translation(grammar_form, text, "Exam in progress")
            choice.addTranslation(translation)
        else:
            # It has shown the full answer
            try:
                task = self.examination.next()
                self.clearExamView()
                self.updateExamView(task)
            except StopIteration:
                QMessageBox.information(self, 
                        "{0} - Exam Finished".format(self.appName),
                        "The Exam has been finished!")
                self.clearExamView()
                self.amountLabel.setText("To Learn: {0}".format(self.words.amountToLearn()))
            except ExaminationException, e:
                QMessageBox.critical(self, 
                    "{0} - Exam Next Task".format(self.appName),
                    unicode(e))
            return
            
        if self.examination.check(choice):
            fullAnswer = self.examination.fullAnswer()
            self.updateExamView(fullAnswer)
        else:
            QMessageBox.critical(self, 
                "{0} - Exam Wrong Answer".format(self.appName),
                "The answer is wrong!")
            if self.examTitleEdit.isEnabled():
                self.examTitleEdit.selectAll()
                self.examTitleEdit.setFocus()
            else:
                self.examGrammarFormComboBox.setCurrentIndex(0)
                self.examTranslationTextEdit.selectAll()
                self.examGrammarFormComboBox.setFocus()
    
    
    @pyqtSignature("")
    def on_examHintButton_clicked(self):
        hint = self.examination.hint()
        self.updateExamView(hint)
        self.examEnterButton.setEnabled(True)
    
    
    @pyqtSignature("QString")
    def on_examTitleEdit_textEdited(self, text):
        if self.examination.current_order == "backward":
            self.examEnterButton.setEnabled(not text.isEmpty())
    
    
    @pyqtSignature("int")
    def on_examGrammarFormComboBox_activated(self, pos):
        if self.examination.current_order == "forward":
            translation_presented = not self.examTranslationTextEdit.toPlainText().isEmpty()
            grammar_form_presented = bool(pos)
            self.examEnterButton.setEnabled(translation_presented and grammar_form_presented)


    @pyqtSignature("")
    def on_examTranslationTextEdit_textChanged(self):
        if self.examination.current_order == "forward":
            translation_presented = not self.examTranslationTextEdit.toPlainText().isEmpty()
            grammar_form_presented = bool(self.examGrammarFormComboBox.currentIndex())
            self.examEnterButton.setEnabled(translation_presented and grammar_form_presented)
    
    
    @pyqtSignature("")
    def on_serviceClearAllScoresAction_triggered(self):
        reply = QMessageBox.warning(self, 
                self.appName, 
                "Clear scores from all words?", 
                QMessageBox.Yes|QMessageBox.No)
        if reply == QMessageBox.Yes:
            self.words.clearAllScores()
            self.updateWordTable()


    def updateEditView(self):
        if self.words.group_name:
            self.groupNameEdit.setText(self.words.group_name)
        if self.words.repeat_word:
            self.repeatWordSpinBox.setValue(self.words.repeat_word)
        if self.words.word_amount_exam:
            self.wordAmountExamSpinBox.setValue(self.words.word_amount_exam)
        self.updateWordTable()
        title = (os.path.basename(self.filename)
                 if self.filename else "NewFile")
        self.setWindowTitle("{0} - {1} (Edit Mode)".format(
                self.appName, title))
        self.amountLabel.setText("Total {0}: / To Learn: {1}".format(len(self.words), self.words.amountToLearn()))
    
    
    def updateExamView(self, respondWord=None):
        title = (self.words.group_name
                 if self.words.group_name else "EmptySet")
        self.setWindowTitle(u"{0} - {1} (Exam Mode)".format(
                self.appName, title))
        
        if not self.examination.isRunning():
            self.examStartButton.setEnabled(True)
            self.examStartButton.setToolTip("")
            self.examStartButton.setStatusTip("")
            self.examStartButton.setText("St&art")
            self.examEnterButton.setEnabled(False)
            self.examHintButton.setEnabled(False)
            self.clearExamView()
            self.amountLabel.setText("To Learn: {0}".format(self.words.amountToLearn()))
            if not self.words:
                msg = "Please add new words in the Edit-Mode or open an existing worddata file."
                QMessageBox.information(self, 
                        "{0} - Exam is Empty".format(self.appName), 
                        "Examination is empty!<br/>{0}".format(msg))
                self.examStartButton.setEnabled(False)
                self.examStartButton.setToolTip(msg)
                self.examStartButton.setStatusTip(msg)
            return
        else:
            self.amountLabel.setText("Passed: {0}/ Remains: {1}".format(
                    self.examination.task_number, 
                    self.examination.remains))
            self.examHintButton.setEnabled(True)
            self.examStartButton.setText("Rest&art")
        
        if respondWord is not None:
            # TODO: may be with checking exam direction and FullAnswer will be extraodinary much easier?
            # here respondWord is either: Task, Hint or FullAnswer. In any case we should fill examview
            respondTranslation = None
            if len(respondWord):
                respondTranslation = respondWord.translationsWithKey[0][1]
            if respondWord.title:
                self.examTitleEdit.setText(respondWord.title)
                self.examTitleEdit.setEnabled(False)
                self.examTranscriptionLabel.setText(u"[{0}]".format(respondTranslation.transcription))
                
                self.examTranslationGroupBox.setEnabled(True)

                respondTranslation = respondWord.translationsWithKey[0][1]
                self.examExampleTextEdit.setPlainText(respondTranslation.example)
                self.examExampleTextEdit.setEnabled(False)
                
                self.examTranslationTextEdit.setEnabled(True)

            
            if respondTranslation is not None:
                if respondTranslation.text:
                    self.examTranslationTextEdit.setPlainText(respondTranslation.text)
                    self.examTranslationTextEdit.setEnabled(False)
                    self.examGrammarFormComboBox.clear()
                    self.examGrammarFormComboBox.addItem(respondTranslation.grammarForm)
                    self.examGrammarFormComboBox.setEnabled(False)
                    self.examTitleEdit.setEnabled(True)
                    self.examTitleEdit.setFocus()
                else:
                    self.examGrammarFormComboBox.addItem("")
                    for form in respondWord.GrammarForms:
                        self.examGrammarFormComboBox.addItem(form)
                    self.examGrammarFormComboBox.setEnabled(True)
                    self.examGrammarFormComboBox.setFocus()

            
            if isinstance(respondWord, FullAnswer):
                self.examTitleEdit.setEnabled(False)
                self.examTranslationGroupBox.setEnabled(False)
                self.examHintButton.setEnabled(False)
                # We need manually increase task_number because it is not actually the amount of passed words
                self.amountLabel.setText("Passed: {0}".format(self.examination.task_number+1))
    
    
    def clearExamView(self):
        self.examTitleEdit.clear()
        self.examTranscriptionLabel.setText("[Transcription]")
        self.examTitleEdit.setEnabled(False)
        self.examGrammarFormComboBox.clear()
        self.examTranslationTextEdit.clear()
        self.examExampleTextEdit.clear()
        self.examTranslationGroupBox.setEnabled(False)
        self.examEnterButton.setEnabled(False)
    
    
    def updateView(self, action):
        if action.text() == self.viewExamAction.text():
            # Switch to Examination mode
            if self.words.isDirty():
                if not self.on_fileSaveAction_triggered():
                    self.viewEditAction.setChecked(True)
                    return
            self.updateExamView()
            self.editToolBar.hide()
            self.examToolBar.show()
            self.mainStackedWidget.setCurrentIndex(1)
        else:
            # Switch to Edit mode
            if self.examination.isRunning():
                reply = QMessageBox.question(self, 
                        "{0} - Change Mode".format(self.appName), 
                        "Do you want to break the current exam?", 
                        QMessageBox.Yes|QMessageBox.No)
                if reply == QMessageBox.No:
                    self.viewExamAction.setChecked(True)
                    return
                else:
                    self.examination.clear()
            if self.words.isDirty():
                self.on_fileSaveAction_triggered()
            self.updateEditView()
            self.examToolBar.hide()
            self.editToolBar.show()
            self.mainStackedWidget.setCurrentIndex(0)
    

    def okToContinue(self):
        if self.examination.isRunning():
            reply = QMessageBox.question(self, 
                    "{0} - Change Mode".format(self.appName), 
                    "Do you want to break the current exam?", 
                    QMessageBox.Yes|QMessageBox.No)
            if reply == QMessageBox.No:
                return False
            else:
                self.examination.clear()

        result = True
        if self.words.isDirty():
            reply = QMessageBox.question(self,
                    "{0} - Unsaved Changes".format(self.appName),
                    "Save unsaved changes?",
                    QMessageBox.Yes|QMessageBox.No|QMessageBox.Cancel)
            if reply == QMessageBox.Cancel:
                return False
            elif reply == QMessageBox.Yes:
                result = self.on_fileSaveAction_triggered()
        if result and self.filename:
            self.last_filename = self.filename[:]
        return result
    
    
    def okToSave(self):
        if self.groupNameEdit.text().isEmpty():
            QMessageBox.critical(self, 
                    "{0} - Save Word Data".format(self.appName), "Group Name should not be empty!")
            self.groupNameEdit.setFocus()
            return False
        if len(self.words) < 1:
            QMessageBox.critical(self, 
                    "{0} - Save Word Data".format(self.appName), "Your word set is empty! Please, add at least one word!")
            return False
        return True
    
    
    def closeEvent(self, event):
        if self.okToContinue():
            settings = QSettings()
            if not self.filename:
                filename = (QVariant(QString(self.last_filename)))
            else:
                filename = (QVariant(QString(self.filename)))
            # We need explicitly operate with toolbar visibility because we always open Application in EDIT mode
            self.editToolBar.show()
            self.examToolBar.hide()
            settings.setValue("LastFile", filename)
            settings.setValue("MainWindow/Geometry", QVariant(self.saveGeometry()))
            settings.setValue("MainWindow/State", QVariant(self.saveState()))
            settings.setValue("Splitter", QVariant(self.editPageSplitter.saveState()))
        else:
            event.ignore()


def main():
    app = QApplication(sys.argv)
    app.setOrganizationName("Eugeun Nad'ok")
    app.setOrganizationDomain("eu.na")
    app.setApplicationName("Learn Words")
    form = MainWindow()
    form.show()
    app.exec_()


main()
