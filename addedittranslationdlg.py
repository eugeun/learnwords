import copy
from PyQt4.QtGui import (QDialog, QMessageBox, QApplication, 
            QTableWidget, QTableWidgetItem)
from PyQt4.QtCore import (Qt, SIGNAL, QString, QVariant, pyqtSignature)
from worddata import WordData, Translation, WordDataException
from ui import Ui_addedittranslationdlg

class AddEditTranslationDlg(QDialog, Ui_addedittranslationdlg.Ui_AddEditTranslationDlg):
    
    def __init__(self, word, translation=None, parent=None):
        """ It can serve as Add or Edit Dialog. Mode depends on self.word, self.editedWord is what actually form is holding
        """
        super(AddEditTranslationDlg, self).__init__(parent)
        self.setupUi(self)
        
        self.word = word
        self.translation = translation

        for form in WordData.GrammarForms:
            self.grammarFormComboBox.addItem(form)
        
        if self.translation is None:
            # Set "Add Word" mode
            self.caption = u"{0} - Add Translation to '{1}'".format(
                    unicode(QApplication.applicationName()), 
                    unicode(self.word.title))
        else:
            # Set "Edit Word" mode
            self.caption = u"{0} - Edit Translation of '{1}'".format(
                    unicode(QApplication.applicationName()), 
                    unicode(self.word.title))
            i = self.grammarFormComboBox.findText(translation.grammarForm)
            if i < 0:
                i = 0
                QMessageBox.critical(self, "Translation Error", 
                        "The grammar form '{0}' is unknown! "
                        "Chosen the first grammar form. Check it!".format(translation.grammarForm))
            self.grammarFormComboBox.setCurrentIndex(i)
            self.scoreSpinBox.setValue(translation.score)
            self.transcriptionEdit.setText(translation.transcription)
            self.translationTextEdit.setPlainText(translation.text)
            self.exampleTextEdit.setPlainText(translation.example)
            self.saveButton.setText("E&dit")

        self.setWindowTitle(self.caption)
    
    
    @pyqtSignature("")
    def on_translationTextEdit_textChanged(self):
        self.updateSaveButton()
    
    
    @pyqtSignature("")
    def on_exampleTextEdit_textChanged(self):
        self.updateSaveButton()
    
    
    def updateSaveButton(self):
        translation_presented = not self.translationTextEdit.toPlainText().isEmpty()
        example_presented = not self.exampleTextEdit.toPlainText().isEmpty()
        
        self.saveButton.setEnabled(translation_presented and example_presented)
    
    
    def accept(self):
        try:
            if self.translation is None:
                translation = Translation(
                    unicode(self.grammarFormComboBox.currentText()), 
                    unicode(self.translationTextEdit.toPlainText()).strip(), 
                    unicode(self.exampleTextEdit.toPlainText()).strip(), 
                    unicode(self.transcriptionEdit.text()).strip(), 
                    self.scoreSpinBox.value())
                self.word.addTranslation(translation)
            else:
                self.word.updateTranslation(
                    self.translation, 
                    unicode(self.grammarFormComboBox.currentText()), 
                    unicode(self.transcriptionEdit.text()).strip(), 
                    self.scoreSpinBox.value(), 
                    unicode(self.translationTextEdit.toPlainText()).strip(), 
                    unicode(self.exampleTextEdit.toPlainText()).strip())
        except WordDataException, e:
            QMessageBox.critical(self, self.caption, unicode(e))
            self.grammarFormComboBox.setFocus()
            return
        QDialog.accept(self)
    
    
    def reject(self):
        translation_changed = False
        if self.saveButton.isEnabled():
            translation = Translation(
                    unicode(self.grammarFormComboBox.currentText()), 
                    unicode(self.translationTextEdit.toPlainText()).strip(), 
                    unicode(self.exampleTextEdit.toPlainText()).strip(), 
                    unicode(self.transcriptionEdit.text()).strip(), 
                    self.scoreSpinBox.value())
            if self.translation is not None:
                # We are trying to edit translation
                if not self.translation == translation:
                    translation_changed = True
            else:
                # We are trying to add translation
                translation_changed = True
        if translation_changed:
            reply = QMessageBox.question(self, 
                    self.caption, 
                    "Save changes?", 
                    QMessageBox.Yes|QMessageBox.No)
            if reply == QMessageBox.Yes:
                self.accept()
                return
        QDialog.reject(self)


if __name__ == "__main__":
    import sys
    from worddata import WordContainer
    app = QApplication(sys.argv)
    word = WordData("Hello")
    translation1 = Translation("noun", "test", "passed")
    translation2 = Translation("verb", "test2", "passed2")
    word.addTranslation(translation1)
    word.addTranslation(translation2)
    word.dirty = False
    print(word.title)
    for translation in word:
        print(u"\t{0}".format(unicode(translation)))
    #form = AddEditTranslationDlg(word)
    form = AddEditTranslationDlg(word, translation1)
    if form.exec_():
        print(word.title)
        for translation in word:
            print(u"\t{0}".format(unicode(translation)))
