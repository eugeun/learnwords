from PyQt4.QtGui import (QWidget, QTabWidget)
from PyQt4.QtCore import (Qt, QVariant)
from ui import Ui_translationwidget

class TranslationWidget(QWidget, Ui_translationwidget.Ui_TranslationWidget):
    
    def __init__(self, title, translation, parent=None):
        super(TranslationWidget, self).__init__(parent)
        self.setupUi(self)
        self.titleEdit.setText(title)
        self.transcriptionEdit.setText(translation.transcription)
        self.scoreEdit.setText(unicode(translation.score))
        self.translationText.setPlainText(translation.text)
        self.examplesText.setPlainText(translation.example)


class TranslationTabs(QTabWidget):
    
    def __init__(self, parent=None):
        super(TranslationTabs, self).__init__(parent)
        self.translationIdFromWidgetId = {}
    
    
    def populate(self, title, translations, edited=None):
        self.clear()
        self.translationIdFromWidgetId = {}
        activateWidget = None
        for translation in translations:
            translationWidget = TranslationWidget(title, translation)
            if edited is not None and edited == id(translation):
                activateWidget = translationWidget
            self.translationIdFromWidgetId[id(translationWidget)] = id(translation)
            self.addTab(translationWidget, translation.grammarForm)
        if activateWidget is not None:
            self.setCurrentWidget(activateWidget)
    
    
    def currentTranslationId(self):
        currentWidget = self.currentWidget()
        if currentWidget is None:
            return None
        try:
            translation_id = self.translationIdFromWidgetId[id(currentWidget)]
        except KeyError:
            return None
        return translation_id


if __name__ == "__main__":
    from PyQt4.QtGui import QApplication
    import sys
    from worddata import Translation, WordData
    app = QApplication(sys.argv)
    word = WordData("Ball")
    translation1 = Translation("noun", "a spherical object or mass of material",  "a ball of wool; he crushed the card into a ball", "bol")
    translation2 = Translation("verb", "squeeze or form (something) into a rounded shape",  "Robert balled up his napkin and threw it on to his plate", "bol")
    translation3 = Translation("verb", "wrap the root ball of (a tree or shrub) in hessian to protect it during transportation",  "the fishing nets eventually ball up and sink", "bol")
    word.addTranslation(translation1).addTranslation(translation2).addTranslation(translation3)
    #form = TranslationWidget(translation)
    form = TranslationTabs()
    form.populate(word.translations())
    form.show()
    app.exec_()
